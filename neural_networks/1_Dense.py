# coding=utf-8
import keras
from keras import layers
from keras import models
from matplotlib import pyplot as plt

""" Sieć jednokierunkowa """

# params
path = r'/content/drive/My Drive/colab_files/TRAIN.csv'
num = 5208                              # liczba wszystkich próbek
val_num = 4168                          # numer pierwszej próbki ze zbioru walidacyjnego
len_x = 1200                            # długość próbki dźwięku
len_y = 2                               # długość oczekiwanego wyjścia sieci
epochs = 50                             # liczba epok
steps_per_epoch = 50                    # liczba kroków w jednej epoce
validation_steps = 10                   # kroki walidacji
optimizer = 'rmsprop'                   # optymalizator
loss = 'mse'                            # funkcja straty
metrics = ['mae']                       # metryka
dropout = 0.0                           # odrzucanie
recurrent_dropout = 0.0                 # odrzucanie rekurencyjne

# przygotowanie danych
x = np.zeros((num, len_x), dtype=np.float32)        # wejście sieci
y = np.zeros((num, len_y), dtype=np.float32)        # wyjście sieci
i = 0                                               # licznik

with open(path) as csv_file:
    csv_data = csv.reader(csv_file, delimiter=',')
    for row in csv_data:
        x[i, :] = row[:-2]
        y[i, :] = row[-2:]
        i += 1

np.random.shuffle(x)                                # rozlosowanie kolejności danych
np.random.shuffle(y)
x_train = x[:val_num, :]                            # dane treningowe
y_train = y[:val_num, :]
x_test = x[val_num:, :]                             # dane walidacyjne
y_test = y[val_num:, :]

# definition of network model
model = models.Sequential()
model.add(layers.Dense(512, activation='relu', input_shape=(len_x,)))
model.add(layers.Dense(128, activation='relu'))
model.add(layers.Dense(len_y))

model.summary()

# kompilacja
model.compile(optimizer=optimizer,
              loss=loss,
              metrics=metrics)

# trenowanie sieci
history = model.fit(x_train, y_train,
                    epochs=epochs,
                    shuffle=True,
                    steps_per_epoch=steps_per_epoch,
                    validation_steps=validation_steps,
                    validation_data=(x_test, y_test))

# zapis do pliku
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
model.save_weights("model.h5")
print("Zapisano model sieci")

# wykres
loss = history.history['loss']              # strata procesu uczenia dla danych treningowych
val_loss = history.history['val_loss']      # strata procesu uczenia dla danych walidacyjnych
epochs = range(len(loss))

plt.figure()
plt.plot(epochs, loss, 'bo', label='Dane treningowe')
plt.plot(epochs, val_loss, 'b', label='Dane walidacyjne')
plt.title('Wartości funkcji straty procesu uczenia')
plt.xlabel('Epoki')
plt.ylabel('Wartość funkcji straty')
plt.legend()
plt.grid(True)

plt.show()