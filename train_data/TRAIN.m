%=====================ZAPIS DO PLIKU===================

probki = zeros(48, 1202);
csv = 'TRAIN.csv';
licz1 = 1;
licz2 = 1;

%==========A==========
pocz = 10000;

for j = 1:5
    filename = num2str(j);
    filename = strcat('a', filename, '.wav');
    [pr, licz2] = f_parse(filename, pocz);
    licz2 = licz1 + licz2 - 1;
    probki(licz1:(licz2 - 1), :) = pr(:, :);
    licz1 = licz2;
end

%==========B==========
pocz = 10000;

for j = 1:27
    filename = num2str(j);
    filename = strcat('b', filename, '.wav');
    if (j == 3)
        pocz = 39000;
    else
        pocz = 10000;
    end
    [pr, licz2] = f_parse(filename,pocz);
    licz2 = licz1 + licz2 - 1;
    probki(licz1:(licz2-1),:) = pr(:,:);
    licz1 = licz2;
end

%==========C==========
pocz = 30000;

for j=1:9
    filename = num2str(j);
    filename = strcat('c',filename,'.wav');
    [pr, licz2] = f_parse(filename,pocz);
    licz2 = licz1 + licz2 - 1;
    probki(licz1:(licz2-1),:) = pr(:,:);
    licz1 = licz2;
end

%==========D==========
pocz = 10000;

for j=1:65
    filename = num2str(j);
    filename = strcat('d',filename,'.wav');
    if (j == 53)
        pocz = 74000;
    end
    if (j == 69)
        pocz = 200000;
    end
    if (j == 70)
        pocz = 200000;
    end
    if (j == 71)
        pocz = 180000;
    end
    if (j == 7)
        pocz = 160000;
    else 
        pocz = 10000;
    end
    [pr, licz2] = f_parse(filename,pocz);
    licz2 = licz1 + licz2 - 1;
    probki(licz1:(licz2-1),:) = pr(:,:);
    licz1 = licz2;
end

%=========CSV=========
csvwrite(csv,probki);