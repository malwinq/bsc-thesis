function [probka,licznik] = f_parse(filename,pocz)

    %============================PARAMETRY=====================================
    bpm = 110;                  % tempo
    nuta = 0.5;                 % dlugosc trwania nuty
    pocz = 10000;               % poczatek zbierania probek
    gr = 0.02;                  % granica pochodnej do wykrywania dzwieku
    T = 1200;                   % ile chw prob ma zawierac jedna probka

    %=============================PROGRAM======================================
    [y,Fs] = audioread(filename);
    y = y(:, 1);

    % dlugosci
    bps = bpm / 60;             % udzerzenia na sekunde
    dl = (bps * Fs) * nuta;     % dlugosc trwania jednego dzwieku
    dl = round(dl);

    prz110 = 23000 / bpm;       % przerwa dla tempa
    prz = prz110 * bpm;         % przerwa w dzwiekach w zaleznosci od tempa
    prz = round(prz);
    prz_max110 = 35000 / bpm;   % maksymalna przerwa miedzy dzwiekami
    prz_max = prz_max110 * bpm;

    % pochodna
    dy = diff(y);

    % zakres zbierania probek
    st = zeros(12, 6);          % wektor zachowujacy nr probek w ktorych
                                % zaczyna sie dzwiek
                                % kolumny - kolejne oktawy
    ok = 1;                     % oktawa
    dz = 0;                     % dzwiek
    j = pocz;
    koniec = pocz + 100000000;  % koniec zbierania probek

    % zbieranie probek
    while(j < koniec)
        % jesli pochodna przekroczy granice
        if((dy(j) > gr) | (dy(j) < -gr))
            if (dz > 0)
                % jesli zacznie sie nowa oktawa
                if (((j - st(dz,ok)) > prz_max) & ((j - st(dz,ok)) < 2*prz))
                    j = st(dz,ok) + 1.5*prz_max;
                    if (dz <= 9)
                        j = st(dz,ok) + 1.5*prz_max + 2*prz;
                    end
                    continue;
                end
                if (((j - st(dz,ok)) > 2*prz) | (dz >= 12))
                    ok = ok + 1;
                    dz = 0;
                end
            end
            dz = dz + 1;
            st(dz,ok) = j;       % nr probki zapisywany do wektora startow
            j = j + prz;         % licznik przeskakuje o przerwe
            if (ok == 6)
                koniec = st(1,6) + 292000;     % dokad zbierac probki
                if (koniec > length(y))
                    koniec = length(y);
                end
            end
        end
        j = j + 1;
    end

    T = T - 1;
    dobrze = 1;
    wek_okt = zeros(1,2);           % wektor, ktory bedzie argumentem petli do zapisu
                                    % do pliku zawieta nry oktaw nagranych poprawnie
    n = 1;

    % sprawdzanie, ktore oktawy dobrze sie nagraly
    for j = 1:6
        for k = 1:12
            if (st(k,j) == 0)
                dobrze = 0;
                break;
            end
        end
        if (dobrze == 1) 
            wek_okt(n) = j;
            n = n + 1;
        end
        dobrze = 1;
    end

    probka = zeros(1,1202);         % macierz probek

    oktawa = 0;                     % wielkosci wyjsciowe sieci
    dzwiek = 0;
    licznik = 1;
    n = 1;

    % zapis do jednej macierzy probek
    for j = 1:6
        if (n > size(wek_okt))
            n = n - 1;
        end
        if (j == wek_okt(n))
            for k = 1:12
                %licznik = (wek_okt - 1) * 12 + k;
                probka(licznik,1:1200) = y(st(k,j):(st(k,j)+T));
                dzwiek = ((1/12) * (k-1)) + (1/24);
                oktawa = ((1/6) * (j - 1)) + (1/12);
                probka(licznik,1201) = dzwiek;
                probka(licznik,1202) = oktawa;
                licznik = licznik + 1;
            end
            n = n + 1;
        end
    end

    % zapis macirzy do pliku csv
    kol = 0;                       % zapis od pierwszej kolumny
    csvwrite('TRAIN.csv',probka,wiersz,kol)

    %{
    audiowrite('test.wav',yn,Fs);
    %test
    dst = diff(st);
    %}
end